package icu.tmux.mres.listeners;

import icu.tmux.mres.utils.NbtUtil;
import icu.tmux.mres.utils.Util;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.PrepareItemCraftEvent;
import org.bukkit.inventory.CraftingInventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.Arrays;
import java.util.Objects;

public final class CraftListener implements Listener {

	private static final String DEFAULT_NAME = "&7Meal, Ready-to-Eat;;";

	@EventHandler
	public void onPrepareItemCraft(PrepareItemCraftEvent event) {
		System.out.println("Called PrepareItemCraft");
		if (event.isRepair() || event.getRecipe() != null) return;

		CraftingInventory inventory = event.getInventory();
		ItemStack[] matrix = inventory.getMatrix();

		ItemStack idItem = getIdItem(matrix);
		if (idItem == null || idItem.getAmount() > 1) return;

		matrix = Arrays.stream(matrix).filter(itemStack -> Objects.nonNull(itemStack) && !itemStack.equals(idItem)).toArray(ItemStack[]::new);
		ItemStack[] packageItems = Arrays.stream(matrix).filter(Util::canBePackaged).toArray(ItemStack[]::new);
		if (!Arrays.equals(matrix, packageItems) || packageItems.length == 0) return;

		ItemMeta idMeta = idItem.getItemMeta();
		String playerName = event.getView().getPlayer().getName();
		String[] displayName = (idMeta.hasDisplayName() ? idMeta.getDisplayName() : DEFAULT_NAME + playerName).split(";;");
		String name = displayName[0], company = displayName.length == 2 ? displayName[1] : playerName;

		System.out.println("Called CraftingInventory#setResult");
		inventory.setResult(Util.buildPkg(name, company, packageItems));
	}

	private ItemStack getIdItem(ItemStack[] matrix) {
		return Arrays.stream(matrix)
				.filter(stack -> NbtUtil.useNbt(stack, tag -> tag.hasKey("MRE-Validator")))
				.peek(itemStack -> {
					ItemMeta itemMeta = itemStack.getItemMeta();
					itemMeta.setDisplayName(null);
					itemStack.setItemMeta(itemMeta);
				})
				.findFirst().orElse(null);
	}

}
