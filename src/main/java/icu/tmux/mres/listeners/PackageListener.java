package icu.tmux.mres.listeners;

import icu.tmux.mres.utils.Config;
import icu.tmux.mres.utils.NbtUtil;
import icu.tmux.mres.utils.Util;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.EquipmentSlot;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;

import java.util.List;
import java.util.Objects;

public final class PackageListener implements Listener {

	@EventHandler
	public void onPackageOpen(PlayerInteractEvent event) {
		if (event.getHand() != EquipmentSlot.HAND) return;
		switch (event.getAction()) {
			case PHYSICAL:
			case LEFT_CLICK_AIR:
			case LEFT_CLICK_BLOCK:
				return;
		}

		ItemStack itemStack = event.getItem();
		if (!isPackage(itemStack)) return;

		Player player = event.getPlayer();
		List<ItemStack> itemStacks = NbtUtil.getItems(itemStack);
		if (itemStacks == null || itemStacks.isEmpty()) {
			player.sendMessage(Util.color("&cUh oh! An error occurred while trying to open your MRE, report this to the admins."));
			return;
		}

		PlayerInventory inventory = player.getInventory();
		inventory.removeItem(itemStack);

		World world = player.getWorld();
		itemStacks.stream().filter(stack -> Objects.nonNull(stack) && stack.getType() != Material.AIR).forEach(stack -> {
			if (!Config.dropItems() && inventory.firstEmpty() != -1) {
				inventory.addItem(stack);
			} else {
				world.dropItemNaturally(player.getLocation(), stack);
			}
		});
	}

	private boolean isPackage(ItemStack itemStack) {
		return Objects.nonNull(itemStack) && itemStack.getType() == Config.pkgMaterial() && NbtUtil.hasCheck(itemStack);
	}

}
