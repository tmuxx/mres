package icu.tmux.mres.utils;

import com.google.common.collect.Maps;
import icu.tmux.mres.MREPlugin;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.Arrays;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.stream.Collectors;

public final class Util {

	private static final EnumMap<Material, Material> COOKABLE_FOODS;

	static {
		COOKABLE_FOODS = Maps.newEnumMap(Material.class);
		COOKABLE_FOODS.putAll(new HashMap<Material, Material>() {{
			put(Material.POTATO_ITEM, Material.BAKED_POTATO);
			put(Material.RAW_CHICKEN, Material.COOKED_CHICKEN);
			put(Material.RAW_BEEF, Material.COOKED_BEEF);
			put(Material.RAW_FISH, Material.COOKED_FISH);
			put(Material.PORK, Material.GRILLED_PORK);
			put(Material.RABBIT, Material.COOKED_RABBIT);
			put(Material.MUTTON, Material.COOKED_MUTTON);
		}});
	}

	public static ItemStack buildPkg(String name, String company, ItemStack[] itemStacks) {
		final String newName = Util.stripFormatting(Util.toComponent(Util.color(name)));
		final String newCompany = Util.stripFormatting(Util.toComponent(Util.color(company)));

		itemStacks = Arrays.stream(itemStacks).peek(itemStack -> {
			if (Config.cookRaw() && COOKABLE_FOODS.containsKey(itemStack.getType())) {
				itemStack.setType(COOKABLE_FOODS.get(itemStack.getType()));
			}
		}).toArray(ItemStack[]::new);

		ItemStack itemStack = new ItemStack(Config.pkgMaterial(), 1, (short) Config.pkgDurability());
		ItemMeta itemMeta = itemStack.getItemMeta();
		itemMeta.setUnbreakable(true);
		itemMeta.addItemFlags(ItemFlag.HIDE_UNBREAKABLE);
		itemMeta.setDisplayName(Config.pkgName().replace("#{name}", newName));
		itemMeta.setLore(Config.pkgLore().stream()
				.map(s -> s.replace("#{name}", newName).replace("#{company}", newCompany))
				.collect(Collectors.toList()));
		itemStack.setItemMeta(itemMeta);
		itemStack = NbtUtil.saveItems(itemStack, itemStacks);
		return NbtUtil.saveNbt(itemStack, tagCompound -> {
			tagCompound.setString("MRE-Name", newName);
			tagCompound.setString("MRE-Company", newCompany);
			tagCompound.setInt("MRE-Validated", MREPlugin.RANDOM.nextInt());
			return tagCompound;
		});
	}

	private static TextComponent toComponent(String string) {
		return new TextComponent(TextComponent.fromLegacyText(string, net.md_5.bungee.api.ChatColor.GRAY));
	}

	public static String color(String string) {
		return ChatColor.translateAlternateColorCodes('&', string);
	}

	public static String colorless(String string) {
		return ChatColor.stripColor(ChatColor.translateAlternateColorCodes('&', string));
	}

	private static String stripFormatting(TextComponent textComponent) {
		textComponent.retain(ComponentBuilder.FormatRetention.FORMATTING);
		textComponent.setBold(false);
		textComponent.setItalic(false);
		textComponent.setObfuscated(false);
		textComponent.setUnderlined(false);
		textComponent.setStrikethrough(false);
		return textComponent.toLegacyText();
	}

	public static boolean canBePackaged(ItemStack itemStack) {
		Material type = itemStack.getType();
		if (Config.blacklistWhitelist()) {
			return !Config.materials().contains(type);
		}
		return Config.materials().contains(type);
	}

}
