package icu.tmux.mres.utils;

import net.minecraft.server.v1_12_R1.NBTTagCompound;
import net.minecraft.server.v1_12_R1.NBTTagList;
import org.bukkit.craftbukkit.v1_12_R1.inventory.CraftItemStack;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

public final class NbtUtil {

	public static ItemStack saveNbt(ItemStack itemStack, Function<NBTTagCompound, NBTTagCompound> nbtFunc) {
		net.minecraft.server.v1_12_R1.ItemStack nmsStack = CraftItemStack.asNMSCopy(itemStack);
		nmsStack.setTag(nbtFunc.apply(nmsStack.hasTag() && nmsStack.getTag() != null ? nmsStack.getTag() : new NBTTagCompound()));
		return CraftItemStack.asBukkitCopy(nmsStack);
	}

	public static <T> T useNbt(ItemStack itemStack, Function<NBTTagCompound, T> nbtFunc) {
		net.minecraft.server.v1_12_R1.ItemStack nmsStack = CraftItemStack.asNMSCopy(itemStack);
		return nbtFunc.apply(nmsStack.hasTag() && nmsStack.getTag() != null ? nmsStack.getTag() : new NBTTagCompound());
	}

	static ItemStack saveItems(ItemStack itemStack, ItemStack[] items) {
		return saveNbt(itemStack, tagCompound -> {
			NBTTagList tagList = new NBTTagList();
			for (ItemStack stack : items) {
				tagList.add(CraftItemStack.asNMSCopy(stack).save(new NBTTagCompound()));
			}

			tagCompound.set("MRE-Items", tagList);
			return tagCompound;
		});
	}

	public static List<ItemStack> getItems(ItemStack itemStack) {
		return useNbt(itemStack, tagCompound -> {
			if (!tagCompound.hasKey("MRE-Items")) throw new IllegalStateException("Items have not been applied.");

			List<ItemStack> itemStacks = new ArrayList<>();
			NBTTagList tagList = (NBTTagList) tagCompound.get("MRE-Items");
			for (int i = 0; i < tagList.size(); i++) {
				NBTTagCompound compound = tagList.get(i);
				itemStacks.add(CraftItemStack.asBukkitCopy(new net.minecraft.server.v1_12_R1.ItemStack(compound)));
			}
			return itemStacks;
		});
	}

	public static boolean hasCheck(ItemStack itemStack) {
		return useNbt(itemStack, tagCompound -> tagCompound.hasKey("MRE-Validated"));
	}

}
