package icu.tmux.mres.utils;

import icu.tmux.mres.MREPlugin;
import org.bukkit.Material;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public final class Config {

	public static Material pkgMaterial() {
		return Material.getMaterial(MREPlugin.get().getConfig().getString("package.type", "LEATHER"));
	}

	static String pkgName() {
		return MREPlugin.get().getConfig().getString("package.name", "#{name}");
	}

	static List<String> pkgLore() {
		final List<String> lore = MREPlugin.get().getConfig().getStringList("package.lore");
		return lore.stream().map(Util::color).collect(Collectors.toList());
	}

	static int pkgDurability() {
		return MREPlugin.get().getConfig().getInt("package.durability", 1);
	}

	public static boolean dropItems() {
		return MREPlugin.get().getConfig().getBoolean("drop-items", false);
	}

	static boolean cookRaw() {
		return MREPlugin.get().getConfig().getBoolean("cook-raw", true);
	}

	static boolean blacklistWhitelist() {
		return MREPlugin.get().getConfig().getBoolean("blacklist-whitelist", false);
	}

	static List<Material> materials() {
		return MREPlugin.get().getConfig().getStringList("items").stream()
				.map(Material::getMaterial).filter(Objects::nonNull).collect(Collectors.toList());
	}

}
