package icu.tmux.mres;

import com.google.common.collect.Lists;
import icu.tmux.mres.listeners.CraftListener;
import icu.tmux.mres.listeners.PackageListener;
import icu.tmux.mres.utils.NbtUtil;
import icu.tmux.mres.utils.Util;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.craftbukkit.v1_12_R1.inventory.CraftItemStack;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;

public final class MREPlugin extends JavaPlugin {

	public static final ThreadLocalRandom RANDOM = ThreadLocalRandom.current();
	private static MREPlugin instance;

	public static MREPlugin get() {
		return instance;
	}

	@Override
	public void onLoad() {
		instance = this;
		getConfig().options().copyDefaults(true);
		saveDefaultConfig();
	}

	@Override
	public void onEnable() {
		Bukkit.getPluginManager().registerEvents(new CraftListener(), this);
		Bukkit.getPluginManager().registerEvents(new PackageListener(), this);
		getCommand("mres").setExecutor(new MRECommand());
	}

	@Override
	public void onDisable() {
		instance = null;
	}

	class MRECommand implements CommandExecutor {

		@Override
		public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
			switch (args[0].toLowerCase()) {
				case "reload":
					if (!sender.hasPermission("mres.reload")) {
						sender.sendMessage(Util.color("&cYou do not have permission to do that."));
						return true;
					}

					reloadConfig();
					sender.sendMessage(Util.color("&aConfiguration file reloaded."));
					return true;

				case "debug":
					if (!(sender instanceof Player)) {
						sender.sendMessage(Util.color("&cOnly players may use this command."));
						return true;
					}

					if (!sender.hasPermission("mres.debug")) {
						sender.sendMessage(Util.color("&cYou do not have permission to do that."));
						return true;
					}

					Player player = (Player) sender;
					PlayerInventory inventory = player.getInventory();

					ItemStack itemStack = inventory.getItemInHand();
					if (itemStack == null) {
						player.sendMessage(Util.color("&cYou need an item in your hand."));
						return true;
					}

					net.minecraft.server.v1_12_R1.ItemStack nmsStack = CraftItemStack.asNMSCopy(itemStack);
					boolean nmsOrItem = nmsStack.hasTag() && nmsStack.getTag() != null;
					player.sendMessage(Util.color("&7Debug Mode: " + (nmsOrItem ? "NBT" : "ItemStack")));
					player.sendMessage(Util.colorless(nmsOrItem ? nmsStack.getTag().toString() : itemStack.toString()));
					return true;

				case "validator":
					if (!(sender instanceof Player)) {
						sender.sendMessage(Util.color("&cOnly players may use this command."));
						return true;
					}

					if (!sender.hasPermission("mres.validator")) {
						sender.sendMessage(Util.color("&cYou do not have permission to do that."));
						return true;
					}

					player = (Player) sender;
					inventory = player.getInventory();

					itemStack = new ItemStack(Material.PAPER);
					ItemMeta itemMeta = itemStack.getItemMeta();
					itemMeta.setDisplayName(Util.color("&7MRE Package Validator"));
					itemMeta.setLore(Lists.newArrayList(
							"&a&oPlace me in the crafting recipe with your MRE recipe!",
							"",
							"&6Rename Format: &e&oPackageName;;CompanyName"
					).stream().map(Util::color).collect(Collectors.toList()));
					itemStack.setItemMeta(itemMeta);
					itemStack = NbtUtil.saveNbt(itemStack, tagCompound -> {
						tagCompound.setInt("MRE-Validator", RANDOM.nextInt());
						return tagCompound;
					});

					inventory.addItem(itemStack);
					return true;

				default:
					return true;
			}
		}

	}

}
